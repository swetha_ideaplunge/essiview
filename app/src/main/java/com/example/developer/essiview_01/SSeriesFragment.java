package com.example.developer.essiview_01;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.developer.essiview_01.adapter.ProductAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SSeriesFragment extends Fragment {

    @BindView(R.id.productRV)
    RecyclerView productRV;

    ProductAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product,container,false);
        ButterKnife.bind(this,view);

        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        productRV.setLayoutManager(manager);

        adapter = new ProductAdapter(getActivity(), new ProductAdapter.ItemClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(getActivity(),ProductDisplayActivity.class);
                intent.putExtra("name", getName(position));
                intent.putExtra("logo",ProductCatalog.sSeries[position]);
                getActivity().startActivity(intent);
            }
        },ProductCatalog.sSeries);
        productRV.setAdapter(adapter);


        return view;
    }

    String getName(int position){
        switch (position){
            case 0:
                return "SSeries";
            case 1:
                return "S4D";
            default:
                return null;
        }
    }
}
