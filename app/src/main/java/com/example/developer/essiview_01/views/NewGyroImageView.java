package com.example.developer.essiview_01.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.SensorEvent;
import android.os.Build;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ScrollView;

public class NewGyroImageView extends AppCompatImageView {

    Context context;

    private int mImageWidth, mImageHeight;

    private float smoothedXValue, smoothedYValue;
    private int mMaxScroll;
    private int mMaxVerticalScroll;

    NewGyroImageView image_view;

    boolean zooming = false;
    private Matrix matrix;
    private Paint paint;
    private Bitmap bitmap;
    private BitmapShader shader;
    private int sizeOfMagnifier = 200;




    public NewGyroImageView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public NewGyroImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public NewGyroImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();

    }

    private void init(){

    //This is required so that onDraw() of this subclass is called
        setWillNotDraw(false);
        image_view = this;

        matrix = new Matrix();

        paint = new Paint();

    }

    public void sensorEvent(SensorEvent event){

        float value = event.values[1];
        float value1 = event.values[0];

        value = value * 50;
        value1 = value1 * 50;

        smoothedXValue = Common.smooth(value, smoothedXValue);
        value = smoothedXValue;

        smoothedYValue = Common.smooth(value1,smoothedYValue);
        value1 = smoothedYValue;

        int scrollX = ((HorizontalScrollView) this.getParent()).getScrollX();
        if (scrollX + value >= mMaxScroll)
            value = mMaxScroll - scrollX;
        if (scrollX + value <= -mMaxScroll)
            value = -mMaxScroll - scrollX;
        ((HorizontalScrollView) this.getParent()).scrollBy((int) value, 0);

        int scrollY = ((ScrollView)(this.getParent()).getParent()).getScrollY();
        if (scrollY + value1 >= mMaxVerticalScroll)
            value1 = mMaxVerticalScroll - scrollY;
        if (scrollY + value1 <= 0)
            value1 = 0;
        ((ScrollView)(this.getParent()).getParent()).scrollBy(0,(int)value1);

    }

    public void setInitialScroll(int distance, double pitchVal){
        if (distance == 1) {
            ((ScrollView) (this.getParent()).getParent()).scrollTo(0, 0);
        } else if (distance == 2) {
            ((ScrollView) (this.getParent()).getParent()).scrollTo(0, 400);
        } else if (distance == 3) {
            ((ScrollView) (this.getParent()).getParent()).scrollTo(0, 1680);
        }

    }

    public NewGyroImageView setImage(BitmapDrawable drawable) {
        Bitmap bmp = resizeBitmap(drawable);
      //  this.setLayoutParams(new ScrollView.LayoutParams(bmp.getWidth(), bmp.getHeight()));
        this.setLayoutParams(new HorizontalScrollView.LayoutParams(bmp.getWidth(), bmp.getHeight()));
        this.setImageBitmap(bmp);
        mMaxScroll = bmp.getWidth();
        mMaxVerticalScroll = bmp.getHeight();

        if (this.getParent() instanceof ScrollView) {
            ((ScrollView) this.getParent()).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true;
                }
            });
        }
        return this;
    }


    private Bitmap resizeBitmap( BitmapDrawable drawable) {
        Bitmap bitmap = drawable.getBitmap();

        mImageWidth = (bitmap.getWidth() * Common.getDeviceHeight(context)) / bitmap.getHeight();
        mImageHeight = (bitmap.getHeight() * Common.getDeviceHeight(context)) / bitmap.getWidth();

        return Bitmap.createScaledBitmap(bitmap, mImageWidth, mImageHeight, true);
    }

    public NewGyroImageView center() {
        image_view.post(new Runnable() {
            @Override
            public void run() {
                if (mImageWidth > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                        ((HorizontalScrollView) image_view.getParent()).setScrollX(mImageWidth / 4);
                    else
                        ((HorizontalScrollView) image_view.getParent()).setX(mImageWidth / 4);
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                        ((HorizontalScrollView) image_view.getParent()).setScrollX(((HorizontalScrollView) image_view.getParent()).getWidth() / 2);
                    else
                        ((HorizontalScrollView) image_view.getParent()).setX(((HorizontalScrollView) image_view.getParent()).getWidth() / 2);
                }
            }
        });
        return image_view;
    }

    public void zoomImage(boolean zoomIn, int scale, int val){
        if (zoomIn){
            ViewGroup.LayoutParams params = getLayoutParams();

            params.height += (val*scale);
            params.width += (val*scale);

            setLayoutParams(params);

            invalidate();

        }else{
            ViewGroup.LayoutParams params = getLayoutParams();

            params.height -= (val*scale);
            params.width -= (val*scale);

            setLayoutParams(params);

            invalidate();

        }
    }

    public void zoomDiff(boolean zoom){
        zooming = zoom;
        Bitmap bmp = ((BitmapDrawable)this.getDrawable()).getBitmap();
        Bitmap[][] bitmaps = splitBitmap(bmp,1,2);

        Bitmap[][] bmps = splitBitmap(bitmaps[0][1],2,1);

        Bitmap bm1 = combineImages(getResizedBmp(bmps[0][0]),getResizedBmp(bmps[1][0]));

        Bitmap bm = combineImages(bitmaps[0][0],bm1);

        this.setImageBitmap(bm);


        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

      /*  if (!zooming) {
            buildDrawingCache();
        } else {

            zooming = false;

            setDrawingCacheEnabled(true);

            measure(MeasureSpec.makeMeasureSpec(0,MeasureSpec.UNSPECIFIED),MeasureSpec.makeMeasureSpec(0,MeasureSpec.UNSPECIFIED));
            layout(0,0,getMeasuredWidth(),getMeasuredHeight());
            buildDrawingCache(true);

            bitmap = loadBitmapFromView(this);

            shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

            paint.setShader(shader);
            matrix.reset();
            matrix.postScale(2f, 2f, 1000, 500);
            paint.getShader().setLocalMatrix(matrix);

          *//*  RectF src = new RectF(100, 100, 150, 150);
            RectF dst = new RectF(0, 100, 100, 100);
            matrix.setRectToRect(src, dst, Matrix.ScaleToFit.CENTER);
            matrix.postScale(2f, 2f);
            paint.getShader().setLocalMatrix(matrix);*//*

          canvas.drawRect(500,800,2000,0,paint);

           // canvas.drawCircle(1000, 500, sizeOfMagnifier, paint);
        }*/
    }

    public static Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap( v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(0, 0, v.getLayoutParams().width, v.getLayoutParams().height);
        v.draw(c);
        return b;
    }

    public Bitmap[][] splitBitmap(Bitmap bitmap, int xCount, int yCount) {
        // Allocate a two dimensional array to hold the individual images.
        Bitmap[][] bitmaps = new Bitmap[xCount][yCount];
        int width, height;
        // Divide the original bitmap width by the desired vertical column count
        width = bitmap.getWidth() / xCount;
        // Divide the original bitmap height by the desired horizontal row count
        height = bitmap.getHeight() / yCount;
        // Loop the array and create bitmaps for each coordinate
        for(int x = 0; x < xCount; ++x) {
            for(int y = 0; y < yCount; ++y) {
                // Create the sliced bitmap
                bitmaps[x][y] = Bitmap.createBitmap(bitmap, x * width, y * height, width, height);
            }
        }
        // Return the array
        return bitmaps;
    }

    public Bitmap combineImages(Bitmap c, Bitmap s) { // can add a 3rd parameter 'String loc' if you want to save the new image - left some code to do that at the bottom
        Bitmap cs = null;

        int width, height = 0;

        /*if(c.getWidth() > s.getWidth()) {
            width = c.getWidth() + s.getWidth();
            height = c.getHeight();
        } else {
            width = s.getWidth() + s.getWidth();
            height = c.getHeight();
        }*/

       /* if(c.getHeight() > s.getHeight()) {
            height = c.getHeight() + s.getHeight();
            width = c.getWidth();
        } else {
            height = s.getHeight() + s.getHeight();
            width = c.getWidth();
        }*/

        height = c.getHeight() + c.getHeight();

        width = c.getWidth();

        cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas comboImage = new Canvas(cs);

        comboImage.drawBitmap(c, 0f, 0f, null);
        comboImage.drawBitmap(s, 0f, c.getHeight(), null);

        return cs;
    }

    private Bitmap getResizedBmp(Bitmap b){
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, b.getWidth(), b.getHeight()), new RectF(0, 0, b.getWidth(), b.getHeight()+500), Matrix.ScaleToFit.CENTER);
        Bitmap bmp = Bitmap.createBitmap(b,0,0,b.getWidth()+100,b.getHeight()+200,m,true);
        //Bitmap bmp = Bitmap.createScaledBitmap(b, b.getWidth()+100, b.getHeight()+200, true);
        return bmp;
    }


}