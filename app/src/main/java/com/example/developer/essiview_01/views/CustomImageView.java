package com.example.developer.essiview_01.views;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomImageView extends AppCompatImageView {

    Context context;
    float mx = 0 , my = 0;
    int maxW = 0, maxH = 0;

    private float smoothedXValue, smoothedYValue;

    public CustomImageView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();

    }

    private void init(){
        SharedPreferences pref = context.getSharedPreferences("Pref",Context.MODE_PRIVATE);
        maxW = pref.getInt("maxX",0);
        maxH = pref.getInt("maxY",0);



    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        this.setTranslationX(smoothedXValue);
        this.setTranslationY(smoothedYValue);

        invalidate();
    }


    public void setOrientationValues(double azimuth, double pitch, double roll){

        mx = (float) pitch;
        my = (float) roll;

        smoothedXValue = Common.smooth(mx,smoothedXValue);
        smoothedYValue = Common.smooth(my,smoothedYValue);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
