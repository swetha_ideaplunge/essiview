package com.example.developer.essiview_01;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdScreenActivity extends AppCompatActivity {

    SliderAdapter adapter;

    @BindView(R.id.adPager)
    ViewPager viewPager;

    @BindView(R.id.adTabs)
    TabLayout adTabs;

    private List<Integer> ids;
    Timer timer;

    @BindView(R.id.skip)
    TextView skip;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_screen);

        ButterKnife.bind(this);

        ids = new ArrayList<>();

        ids.add(R.drawable.no_1_medal_30);
        ids.add(R.drawable.inventor_medal_235x418);
        ids.add(R.drawable.liveoptics_medal_235x418);

        viewPager.setAdapter(new SliderAdapter(this,ids));
        adTabs.setupWithViewPager(viewPager,true);

        timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 2000, 4000);


    }




    public class SliderAdapter extends PagerAdapter {

        private Context context;
        private List<Integer> idList;


        public SliderAdapter(Context context, List<Integer> list) {
            this.context = context;
            this.idList = list;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.item_slider, null);

            ImageView medalIV = view.findViewById(R.id.medalIV);

            medalIV.setImageResource(idList.get(position));

            ViewPager viewPager = (ViewPager) container;
            viewPager.addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ViewPager viewPager = (ViewPager) container;
            View view = (View) object;
            viewPager.removeView(view);
        }
    }


    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            AdScreenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() < ids.size() - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    } else {

                        goToGenderSelection();
                    }
                }
            });
        }
    }

    @OnClick(R.id.skip)
    public void skiptoNext(){
        goToGenderSelection();
    }



    public void goToGenderSelection(){
        timer.cancel();
        Intent intent = new Intent(AdScreenActivity.this,GenderSelectionActivity.class);
        startActivity(intent);
        finish();
    }

}
