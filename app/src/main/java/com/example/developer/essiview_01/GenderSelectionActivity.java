package com.example.developer.essiview_01;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.developer.essiview_01.adapter.GenderAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenderSelectionActivity extends AppCompatActivity {

    @BindView(R.id.genderViewPager)
    ViewPager viewPager;

    GenderAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gender);
        ButterKnife.bind(this);

        adapter = new GenderAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(0);


    }
}
