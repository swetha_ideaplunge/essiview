package com.example.developer.essiview_01.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.developer.essiview_01.R;

public class FeatureAdapter extends RecyclerView.Adapter<FeatureAdapter.FeatureHolder> {

    Context context;

    public FeatureAdapter(Context ctx){
        this.context = ctx;
    }


    @NonNull
    @Override
    public FeatureHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_feature_item,parent,false);
        return new FeatureHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeatureHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    class FeatureHolder extends RecyclerView.ViewHolder{

        public FeatureHolder(View itemView) {
            super(itemView);
        }
    }
}
