package com.example.developer.essiview_01;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.AsyncLayoutInflater;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.essiview_01.views.BlurImage;
import com.example.developer.essiview_01.views.Common;
import com.example.developer.essiview_01.views.CustomImageView;
import com.example.developer.essiview_01.views.CutLayout;
import com.example.developer.essiview_01.views.InnerLayout;
import com.example.developer.essiview_01.views.NewGyroImageView;

import jp.wasabeef.blurry.Blurry;

public class SimulationComparison extends AppCompatActivity implements SensorEventListener {


    Context context;
    String category_name = "Category";
    String name = "";

    AsyncLayoutInflater inflater;

    LinearLayout asyncLayout1, asyncLayout2;
    CutLayout cutLayout1, cutLayout2;

    NewGyroImageView rawGyroImg1, rawGyroImg2, blurGyroImg1, blurGyroImg2, innerImage1, innerImage2;
    InnerLayout innerLayout1, innerLayout2;


    private final float[] mAccelerometerReading = new float[3];
    private final float[] mMagnetometerReading = new float[3];

    private final float[] mRotationMatrix = new float[9];
    private final float[] mOrientationAngles = new float[3];
    private final float[] mOutRotationMatrix = new float[9];

    double azimuth, pitch, roll;

    SensorManager sensorManager;
    Sensor gyroscope, accelerometer, magnetic_field;

    Bitmap bmp;

    boolean loaded = false, show = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = SimulationComparison.this;

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetic_field = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        BitmapDrawable drawable = (BitmapDrawable) ContextCompat.getDrawable(this,R.drawable.simulation);
        bmp = resizeBitmap(drawable);

        if (gyroscope != null) {
            setContentView(R.layout.activity_simulation_comparison);

            asyncLayout1 = findViewById(R.id.asyncLayout1);
            asyncLayout2 = findViewById(R.id.asyncLayout2);

            inflater = new AsyncLayoutInflater(this);
            inflater.inflate(R.layout.async_gyro, null, new AsyncLayoutInflater.OnInflateFinishedListener() {
                @Override
                public void onInflateFinished(View view, int resid, ViewGroup parent) {
                    asyncLayout1.addView(view);
                    rawGyroImg1 = view.findViewById(R.id.raw_image);
                    blurGyroImg1 = view.findViewById(R.id.blur_image);
                    cutLayout1 = view.findViewById(R.id.cutLayout);
                    innerImage1 = view.findViewById(R.id.inner_image);
                    innerLayout1 = view.findViewById(R.id.innerLayout);

                /*    InnerLayout add = view.findViewById(R.id.addLayout);
                    add.setVisibility(View.GONE);*/

                    BitmapDrawable bmp1 = (BitmapDrawable) ContextCompat.getDrawable(SimulationComparison.this, R.drawable.simulation);

                    rawGyroImg1.setImage(bmp1);
                    blurGyroImg1.setImage(bmp1);
                    innerImage1.setImage(bmp1);

                    BlurImage.with(SimulationComparison.this)
                            .radius(1)
                            .sampling(1)
                            .async()
                            .from(bmp)
                            .into(rawGyroImg1);

                    BlurImage.with(SimulationComparison.this)
                            .radius(25)
                            .sampling(1)
                            .async()
                            .from(bmp)
                            .into(blurGyroImg1);

                    BlurImage.with(SimulationComparison.this)
                            .radius(7)
                            .sampling(1)
                            .async()
                            .from(bmp)
                            .into(innerImage1);

                }
            });

            inflater.inflate(R.layout.async_gyro, null, new AsyncLayoutInflater.OnInflateFinishedListener() {
                @Override
                public void onInflateFinished(View view, int resid, ViewGroup parent) {
                    asyncLayout2.addView(view);
                    rawGyroImg2 = view.findViewById(R.id.raw_image);
                    blurGyroImg2 = view.findViewById(R.id.blur_image);
                    cutLayout2 = view.findViewById(R.id.cutLayout);
                    innerLayout2 = view.findViewById(R.id.innerLayout);
                innerImage2 = view.findViewById(R.id.inner_image);


                  /*      InnerLayout add = view.findViewById(R.id.addLayout);
                    add.setVisibility(View.GONE);
*/
                    BitmapDrawable bmp1 = (BitmapDrawable) ContextCompat.getDrawable(SimulationComparison.this, R.drawable.simulation);

                    rawGyroImg2.setImage(bmp1);
                    blurGyroImg2.setImage(bmp1);
                    innerImage2.setImage(bmp1);

                    BlurImage.with(SimulationComparison.this)
                            .radius(1)
                            .sampling(1)
                            .async()
                            .from(bmp)
                            .into(rawGyroImg2);

                    BlurImage.with(SimulationComparison.this)
                            .radius(25)
                            .sampling(1)
                            .async()
                            .from(bmp)
                            .into(blurGyroImg2);

                    BlurImage.with(SimulationComparison.this)
                            .radius(15)
                            .sampling(1)
                            .async()
                            .from(bmp)
                            .into(innerImage2);


                    setCategory();

                    innerLayout2.setResourceID(R.drawable.standard_lens_mask);

                    loaded = true;
                }
            });
        } else {

            setContentView(R.layout.activity_simulation_comparison);

            Toast.makeText(this, "Simulation not possible as the device doesn't support Gyroscope ", Toast.LENGTH_SHORT).show();
        }

        Bundle args = getIntent().getExtras();
        if (args != null)
            name = args.getString(category_name);

        ImageView simulation = findViewById(R.id.simulation);
        ImageView comparison = findViewById(R.id.comparison);
        ImageView video = findViewById(R.id.video);

        ImageView show_hide = findViewById(R.id.show_hide);

        show_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!show) {
                    cutLayout1.setOuterLensID(R.drawable.lens_outer);
                    cutLayout2.setOuterLensID(R.drawable.lens_outer);
                    show = true;
                } else {
                    setCategory();
                    show = false;
                }
            }
        });

        TextView label = findViewById(R.id.label);
        label.setText(name);

        ImageView back = findViewById(R.id.backIV);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToProducts();
            }
        });

        simulation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoSimulation();
            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goTtoVideo();
            }
        });


    }

    public void goToProducts() {
        finish();
    }


    public void gotoSimulation() {

        Intent intent = new Intent(context, ProductSimulation.class);
        intent.putExtra(category_name, name);
        context.startActivity(intent);
        finish();
    }


    public void goTtoVideo() {
        Intent intent = new Intent(context, ProductVideoActivity.class);
        intent.putExtra(category_name, name);
        context.startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(this, magnetic_field, SensorManager.SENSOR_DELAY_GAME);


    }

    @Override
    protected void onStop() {
        super.onStop();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (gyroscope != null) {

            if (sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
                if (loaded) {
                    rawGyroImg1.sensorEvent(sensorEvent);
                    blurGyroImg1.sensorEvent(sensorEvent);
                    innerImage1.sensorEvent(sensorEvent);

                    rawGyroImg2.sensorEvent(sensorEvent);
                    blurGyroImg2.sensorEvent(sensorEvent);
                    innerImage2.sensorEvent(sensorEvent);
                }

            }

        }
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                for (int i = 0; i < 3; i++) {
                    mAccelerometerReading[i] = sensorEvent.values[i];
                }
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                for (int i = 0; i < 3; i++) {
                    mMagnetometerReading[i] = sensorEvent.values[i];
                }
                break;
        }

        boolean success = SensorManager.getRotationMatrix(mRotationMatrix, null, mAccelerometerReading, mMagnetometerReading);

        if (success) {
            boolean succ = SensorManager.remapCoordinateSystem(mRotationMatrix, SensorManager.AXIS_X, SensorManager.AXIS_Y, mOutRotationMatrix);
            if (succ)
                SensorManager.getOrientation(mOutRotationMatrix, mOrientationAngles);
            else
                SensorManager.getOrientation(mRotationMatrix, mOrientationAngles);

            azimuth = Math.toDegrees(mOrientationAngles[0]);
            pitch = Math.toDegrees(mOrientationAngles[1]);
            roll = Math.toDegrees(mOrientationAngles[2]);

            if (loaded) {


              /*  if (pitch > -45) {
                    innerLayout1.setResourceID(1,false);
                    innerLayout2.setResourceID(1,false);
                } else if (pitch < -45 && pitch > -65) {
                    innerLayout1.setResourceID(4,false);
                    innerLayout2.setResourceID(4,false);
                } else if (pitch < -65) {
                    innerLayout1.setResourceID(5,false);
                    innerLayout2.setResourceID(5,false);
                }*/

            }


        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void setCategory() {
        if (category_name.contains("Digital")) {
            cutLayout1.setOuterLensID(R.drawable.outer_lens_3);
            cutLayout2.setOuterLensID(R.drawable.outer_lens_1);
        } else {
            cutLayout1.setOuterLensID(R.drawable.outer_lens_2);
            cutLayout2.setOuterLensID(R.drawable.outer_lens_1);
        }
    }

    private Bitmap resizeBitmap( BitmapDrawable drawable) {
        Bitmap bitmap = drawable.getBitmap();

        int mImageWidth = (bitmap.getWidth() * Common.getDeviceHeight(context)) / bitmap.getHeight();
        int mImageHeight = (bitmap.getHeight() * Common.getDeviceHeight(context)) / bitmap.getWidth();

        return Bitmap.createScaledBitmap(bitmap, mImageWidth, mImageHeight, true);
    }

}
