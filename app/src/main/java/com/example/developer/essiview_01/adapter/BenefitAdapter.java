package com.example.developer.essiview_01.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.developer.essiview_01.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BenefitAdapter extends RecyclerView.Adapter<BenefitAdapter.BenefitViewHolder> {


    Context context;
    List<String> benefitList;
    List<Integer> iconList;

    public BenefitAdapter(Context ctx, List<String> list, List<Integer> list1){
        this.context = ctx;
        this.benefitList = list;
        this.iconList = list1;
    }


    @NonNull
    @Override
    public BenefitViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_benefit_item,parent,false);
        return new BenefitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BenefitViewHolder holder, int position) {

        String name = benefitList.get(position);
        int id = iconList.get(position);

        holder.benefit_icon.setImageResource(id);

        holder.benefit_text.setText(name);


    }

    @Override
    public int getItemCount() {
        return benefitList.size();
    }

    class BenefitViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.benefit_text)
        TextView benefit_text;

        @BindView(R.id.benefit_icon)
        ImageView benefit_icon;

        public BenefitViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
