package com.example.developer.essiview_01;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MaleFragment extends Fragment {

    @BindView(R.id.gender_icon)
    ImageView gender_icon;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_male,container,false);

        ButterKnife.bind(this,view);
        return view;
    }

    @OnClick({R.id.gender_icon,R.id.skipBtn})
    public void goToAgeSelector(){
        Intent intent = new Intent(getActivity(),AgeSelectionActivity.class);
        getActivity().startActivity(intent);
    }
}
