package com.example.developer.essiview_01;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductListingActivity extends AppCompatActivity {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    PageAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_product_listing);

        ButterKnife.bind(this);

        String[] names = getResources().getStringArray(R.array.product_tab_names);

        adapter = new PageAdapter(getSupportFragmentManager(),names);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TextView tv=(TextView) LayoutInflater.from(this).inflate(R.layout.tab_layout,null);
            tv.setText(names[i]);
            if(i==0)
                tv.setTextColor(getResources().getColor(R.color.text_dark_grey));
            tabLayout.getTabAt(i).setCustomView(tv);

        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TextView textView = tab.getCustomView().findViewById(R.id.text1);
                textView.setTextColor(getResources().getColor(R.color.text_dark_grey));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView textView = tab.getCustomView().findViewById(R.id.text1);
                textView.setTextColor(getResources().getColor(R.color.trans_text_dark_grey));

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    class PageAdapter extends FragmentStatePagerAdapter {
        String[] tabNames;

        public PageAdapter(FragmentManager fm, String[] names) {
            super(fm);
            tabNames = names;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new ClassicFragment();
                case 1:
                    return new ESeriesFragment();
                case 2:
                    return new SSeriesFragment();
                case 3:
                    return new XSeriesFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

    }
}
