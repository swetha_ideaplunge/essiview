package com.example.developer.essiview_01;


import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v4.view.AsyncLayoutInflater;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.essiview_01.views.BlurImage;
import com.example.developer.essiview_01.views.Common;
import com.example.developer.essiview_01.views.CustomImageView;
import com.example.developer.essiview_01.views.CutLayout;
import com.example.developer.essiview_01.views.InnerLayout;
import com.example.developer.essiview_01.views.NewGyroImageView;
import com.example.developer.essiview_01.views.SeekbarWithIntervals;
import com.fenchtose.tooltip.Tooltip;

import java.util.ArrayList;
import java.util.List;


public class ProductSimulation extends AppCompatActivity implements SensorEventListener,LoaderManager.LoaderCallbacks<View>, SettingsFragment.SettingsInterface {


    Context context;
    String category_name = "Category";
    String sub_category;
    String name = "";

    CustomImageView raw_image, blur_image, inner_image;
    NewGyroImageView rawGyroImage, blurGyroImage, innerImage;//,addImg;
    InnerLayout innerLayout;//,addLayout;
    CutLayout cutLayout;
    ImageView layer, settings;
    RelativeLayout root;
    TextView near, mid, far;

    LinearLayout asyncLayout;

    private final float[] mAccelerometerReading = new float[3];
    private final float[] mMagnetometerReading = new float[3];

    private final float[] mRotationMatrix = new float[9];
    private final float[] mOrientationAngles = new float[3];
    private final float[] mOutRotationMatrix = new float[9];

    double azimuth, pitch, roll;


    int currentAddValue = 0, currentDiffValue = 0;

    int blur_radius =1;

    SensorManager sensorManager;
    Sensor gyroscope, accelerometer, magnetic_field;
    AsyncLayoutInflater inflater;


    boolean loaded = false, show = true;

    Tooltip tooltip;

    Bitmap bmp;
    int position = 0;
    int index = 0, currentIndex = 0;

    private static final int VIEW_LOADER_ID = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = ProductSimulation.this;

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetic_field = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        Bundle args = getIntent().getExtras();
        if (args != null)
            name = args.getString(category_name);


        //getResId(name);

        BitmapDrawable drawable = (BitmapDrawable) ContextCompat.getDrawable(this,R.drawable.simulation);
        bmp = resizeBitmap(drawable);

        LoaderManager loaderManager = getSupportLoaderManager();

        if (gyroscope != null) {

            setContentView(R.layout.activity_product_simulation);

            asyncLayout = findViewById(R.id.asyncLayout);

            loaderManager.initLoader(VIEW_LOADER_ID,null,this);

        } else {
            Toast.makeText(ProductSimulation.this, "Simulation not possible as the device doesn't support Gyroscope ", Toast.LENGTH_SHORT).show();
        }


        ImageView simulation = findViewById(R.id.simulation);
        ImageView comparison = findViewById(R.id.comparison);
        final ImageView video = findViewById(R.id.video);
        ImageView back = findViewById(R.id.backIV);

        final ImageView show_hide = findViewById(R.id.show_hide);
        ImageView reset = findViewById(R.id.reset);

      /*  show_hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!show) {
                   cutLayout.setOuterLensID(R.drawable.lens_new_outer);
                    show = true;
                    show_hide.setImageResource(R.drawable.show_hide_icon);
                } else {
                    setCategory(position, sub_category);
                    //  show_hide.setImageResource(R.drawable.show_hide_selected);
                    show = false;
                }
            }
        });*/

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              /*  seekbarWithIntervals1.setProgress(0);
                seekbarWithIntervals2.setProgress(0);*/

            }
        });

        near = findViewById(R.id.near);
        mid = findViewById(R.id.mid);
        far = findViewById(R.id.far);
        settings = findViewById(R.id.settingsIcon);
        root = findViewById(R.id.root);


        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                DialogFragment dialogFragment = new SettingsFragment();
                Bundle args = new Bundle();
                args.putInt("ADD",currentAddValue);
                args.putInt("PDiff",currentDiffValue);
                dialogFragment.setArguments(args);
                dialogFragment.show(ft, "dialog");


            }
        });




        comparison.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoComparison();
            }
        });

        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goTtoVideo();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToProducts();
            }
        });


    }


    public void goToProducts() {
        finish();
    }


    public void gotoComparison() {

        Intent intent = new Intent(context, SimulationComparison.class);
        intent.putExtra(category_name, sub_category);
        context.startActivity(intent);
        finish();
    }


    public void goTtoVideo() {

        Intent intent = new Intent(context, ProductVideoActivity.class);
        intent.putExtra(category_name, name);
        context.startActivity(intent);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(loaded) {
            sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_GAME);
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
            sensorManager.registerListener(this, magnetic_field, SensorManager.SENSOR_DELAY_GAME);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                for (int i = 0; i < 3; i++) {
                    mAccelerometerReading[i] = sensorEvent.values[i];
                }
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                for (int i = 0; i < 3; i++) {
                    mMagnetometerReading[i] = sensorEvent.values[i];
                }
                break;
        }

        boolean success = SensorManager.getRotationMatrix(mRotationMatrix, null, mAccelerometerReading, mMagnetometerReading);

        if (success) {
            boolean succ = SensorManager.remapCoordinateSystem(mRotationMatrix, SensorManager.AXIS_X, SensorManager.AXIS_Y, mOutRotationMatrix);
            if (succ)
                SensorManager.getOrientation(mOutRotationMatrix, mOrientationAngles);
            else
                SensorManager.getOrientation(mRotationMatrix, mOrientationAngles);

            azimuth = Math.toDegrees(mOrientationAngles[0]);
            pitch = Math.toDegrees(mOrientationAngles[1]);
            roll = Math.toDegrees(mOrientationAngles[2]);

            if (loaded) {

                if (pitch > -20) {
                    /*index =1;
                    if(index != currentIndex)
                        setDiffImage();
                    addLayout.setResourceID(1,true);
                    innerLayout.setResourceID(1,false);*/
                    near.setTextColor(ContextCompat.getColor(ProductSimulation.this,R.color.text_light_blue));
                    mid.setTextColor(ContextCompat.getColor(ProductSimulation.this,R.color.white));
                    far.setTextColor(ContextCompat.getColor(ProductSimulation.this,R.color.white));
                } else if (pitch < -20 && pitch > -30) {
                    /*index =2;
                    if(index != currentIndex)
                        setDiffImage();
                    addLayout.setResourceID(2,true);
                    innerLayout.setResourceID(2,false);*/
                    near.setTextColor(ContextCompat.getColor(ProductSimulation.this,R.color.white));
                    mid.setTextColor(ContextCompat.getColor(ProductSimulation.this,R.color.text_light_blue));
                    far.setTextColor(ContextCompat.getColor(ProductSimulation.this,R.color.white));
                } else if (pitch < -30) {
                    /*index =3;
                    if(index != currentIndex)
                        setDiffImage();
                    addLayout.setResourceID(3,true);
                    innerLayout.setResourceID(3,false);*/
                    near.setTextColor(ContextCompat.getColor(ProductSimulation.this,R.color.white));
                    mid.setTextColor(ContextCompat.getColor(ProductSimulation.this,R.color.white));
                    far.setTextColor(ContextCompat.getColor(ProductSimulation.this,R.color.text_light_blue));
                }

            }

        }

        if (gyroscope != null) {
            if (loaded) {
                if (sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
                    rawGyroImage.sensorEvent(sensorEvent);
                    blurGyroImage.sensorEvent(sensorEvent);
                    innerImage.sensorEvent(sensorEvent);
                        /*addImg.sensorEvent(sensorEvent);*/
                }
            }
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }




    public void setCategory(int pos, String name) {
        position = pos;
        sub_category = name;
        if (position == 0) {
            cutLayout.setOuterLensID(R.drawable.outer_lens_3);

        } else if (position == 1) {
            cutLayout.setOuterLensID(R.drawable.outer_lens_2);
        }else{
            cutLayout.setOuterLensID(R.drawable.outer_lens_3);
        }
    }

    void getResId(String resName) {

        if (resName.contains("Liberty")) {
            //stringResID = R.array.Liberty_sub;
            blur_radius = 20;
        }
        else if (resName.contains("Comfort")) {
            //stringResID = R.array.Comfort_sub;
            blur_radius = 18;
        }
        else if (resName.contains("Physio")) {
            //stringResID = R.array.Physio_sub;
            blur_radius = 15;
        }
        else if (resName.contains("E Series")) {
          //  stringResID = R.array.E_series_sub;
            blur_radius = 12;
        }
        else if (resName.contains("X Design")) {
           // stringResID = R.array.x_dsign_sub;
            blur_radius = 10;
        }
        else if (resName.contains("Xclusive")) {
          //  stringResID = R.array.xclusive_sub;
            blur_radius = 8;
        }
        else if (resName.contains("X Track")) {
          //  stringResID = R.array.x_track_sub;
            blur_radius = 4;
        }
    }

    @Override
    public Loader<View> onCreateLoader(int id, Bundle args) {
        return new ViewLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<View> loader, View view) {

        asyncLayout.addView(view);
        rawGyroImage = view.findViewById(R.id.raw_image);
        blurGyroImage = view.findViewById(R.id.blur_image);

       /* addImg = view.findViewById(R.id.add_img);
         addLayout = view.findViewById(R.id.addLayout);*/

        innerImage = view.findViewById(R.id.inner_image);
        innerLayout = view.findViewById(R.id.innerLayout);

        cutLayout = view.findViewById(R.id.cutLayout);

        /*addLayout.setResourceID(R.drawable.inner_lens_mask);*/

        final ScrollView scrollView = view.findViewById(R.id.scrollView);

        scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                /*if (pitch > -45){
                    rawGyroImage.setInitialScroll(3,pitch);
                    blurGyroImage.setInitialScroll(3,pitch);
                    innerImage.setInitialScroll(3,pitch);
                    *//*  addImg.setInitialScroll(3,pitch);*//*
                }else if (pitch < -45 && pitch > -65 ){
                    rawGyroImage.setInitialScroll(2,pitch);
                    blurGyroImage.setInitialScroll(2,pitch);
                   innerImage.setInitialScroll(2,pitch);
                    *//*   addImg.setInitialScroll(2,pitch);*//*
                }else if (pitch < -65){
                    rawGyroImage.setInitialScroll(1,pitch);
                    blurGyroImage.setInitialScroll(1,pitch);
                    innerImage.setInitialScroll(1,pitch);
                    *//*   addImg.setInitialScroll(1,pitch);*//*
                }*/
                scrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                loaded = true;
                onResume();

            }
        });


        BitmapDrawable bmp1 = (BitmapDrawable) ContextCompat.getDrawable(ProductSimulation.this, R.drawable.simulation);

        rawGyroImage.setImage(bmp1);
        blurGyroImage.setImage(bmp1);
        innerImage.setImage(bmp1);
         /*addImg.setImage(bmp1);*/



        blurImages();

    }

    @Override
    public void onLoaderReset(Loader<View> loader) {
        asyncLayout.removeAllViews();
    }

    @Override
    public void sendValues(int add, int diff) {

        if (add > currentAddValue) {
            rawGyroImage.zoomImage(true,add- currentAddValue,50);
            innerImage.zoomImage(true,add- currentAddValue,50);
            //addImg.zoomImage(true,add- currentAddValue,50);
            currentAddValue = add;
        } else if (add < currentAddValue) {
            rawGyroImage.zoomImage(false, currentAddValue -add,50);
            innerImage.zoomImage(false, currentAddValue -add,50);
            //addImg.zoomImage(false, currentAddValue -add,50);
            currentAddValue = add;
        }

        if (diff > currentDiffValue) {
            rawGyroImage.zoomDiff(true);
            innerImage.zoomDiff(true);
            currentDiffValue = diff;
        } else if (diff < currentDiffValue) {
            rawGyroImage.zoomDiff(true);
            innerImage.zoomDiff(true);
            currentDiffValue = diff;
        }


    }


    static class ViewLoader extends AsyncTaskLoader<View> {

        Context context;

        public ViewLoader(Context ctx){
            super(ctx);
            this.context = ctx;
        }

        @Override
        protected void onStartLoading() {
            forceLoad();
        }

        @Override
        public View loadInBackground() {
            return LayoutInflater.from(context).inflate(R.layout.async_gyro,null,false);
        }
    }

    void blurImages(){

        BlurImage.with(ProductSimulation.this)
                .radius(25)
                .sampling(1)
                .async()
                .from(bmp)
                .into(blurGyroImage);


        BlurImage.with(ProductSimulation.this)
                .radius(1)
                .sampling(1)
                .async()
                .from(bmp)
                .into(rawGyroImage);


        BlurImage.with(ProductSimulation.this)
                .radius(7)
                .sampling(1)
                .async()
                .from(bmp)
                .into(innerImage);

    }


    private Bitmap resizeBitmap( BitmapDrawable drawable) {
        Bitmap bitmap = drawable.getBitmap();

        int mImageWidth = (bitmap.getWidth() * Common.getDeviceHeight(context)) / bitmap.getHeight();
        int mImageHeight = (bitmap.getHeight() * Common.getDeviceHeight(context)) / bitmap.getWidth();

        return Bitmap.createScaledBitmap(bitmap, mImageWidth, mImageHeight, true);
    }



}
