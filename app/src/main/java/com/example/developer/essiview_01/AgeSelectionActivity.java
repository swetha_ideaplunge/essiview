package com.example.developer.essiview_01;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;


import com.shawnlin.numberpicker.NumberPicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AgeSelectionActivity extends AppCompatActivity {

    @BindView(R.id.continue_btn)
    Button continue_btn;

    @BindView(R.id.agePicker)
    NumberPicker picker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.age_selector);

        ButterKnife.bind(this);


    }

    @OnClick({R.id.continue_btn,R.id.skipBtn})
    public void goToProductList(){
        Log.i("AgeSelection","Selected Age - "+picker.getValue());
        Intent intent = new Intent(AgeSelectionActivity.this,ProductListingActivity.class);
        startActivity(intent);
    }
}
