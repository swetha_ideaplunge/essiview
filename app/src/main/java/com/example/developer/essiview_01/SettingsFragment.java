package com.example.developer.essiview_01;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.developer.essiview_01.views.SeekbarWithIntervals;

import java.util.ArrayList;
import java.util.List;

public class SettingsFragment extends DialogFragment {

    SeekbarWithIntervals seekbarWithIntervals1, seekbarWithIntervals2;

    int addVal = 0, pdVal =0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_box_layout, container, false);


        seekbarWithIntervals1 = v.findViewById(R.id.seekbar_with_intervals1);
        seekbarWithIntervals2 = v.findViewById(R.id.seekbar_with_intervals2);

        TextView save = v.findViewById(R.id.saveBtn);
        TextView cancel = v.findViewById(R.id.cancelBtn);

        Bundle args = getArguments();
        if (args != null){
            addVal = args.getInt("ADD");
            pdVal = args.getInt("PDiff");
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                ((ProductSimulation)getActivity()).sendValues(addVal,pdVal);
                dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        seekbarWithIntervals1.setIntervals(getIntervals());
        seekbarWithIntervals2.setIntervals(getPDIntervals());

        seekbarWithIntervals1.setProgress(addVal);
        seekbarWithIntervals2.setProgress(pdVal);

        seekbarWithIntervals1.setTextColor();
        seekbarWithIntervals2.setTextColor();

        seekbarWithIntervals1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                addVal = i;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        seekbarWithIntervals2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                pdVal = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return v;
    }


    private List<String> getIntervals() {
        return new ArrayList<String>() {{
            add("0");
            add("1");
            add("2");
            add("3");
        }};
    }

    private List<String> getPDIntervals() {
        return new ArrayList<String>() {{
            add("N");
            add("L");
            add("M");
            add("H");
        }};
    }

    public interface SettingsInterface{

        public void sendValues(int add, int diff);
    }
}
