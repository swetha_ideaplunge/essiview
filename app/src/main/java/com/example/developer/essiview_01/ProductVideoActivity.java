package com.example.developer.essiview_01;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

public class ProductVideoActivity extends AppCompatActivity {


    Context context;
    String category_name = "Category";
    String name = "";

    VideoView videoView;

    TextView product_label, standard_label;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = ProductVideoActivity.this;

        setContentView(R.layout.activity_video);

        Bundle args = getIntent().getExtras();
        if (args != null)
            name = args.getString(category_name);

        ImageView simulation = findViewById(R.id.simulation);
        ImageView comparison = findViewById(R.id.comparison);
        final ImageView video = findViewById(R.id.video);

        ImageView video_comparison = findViewById(R.id.video_comparison);
        video_comparison.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductVideoActivity.this,VideoComparisonActivity.class);
                intent.putExtra(category_name, name);
                startActivity(intent);
                //finish();
            }
        });

        videoView = findViewById(R.id.videoView);

        videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.small));

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                videoView.start();
                mediaPlayer.setVolume(0,0);
                mediaPlayer.setLooping(true);
            }
        });

        product_label = findViewById(R.id.product_label);
        product_label.setText(name);

        product_label.setTextColor(getResources().getColor(R.color.text_light_blue));

        standard_label = findViewById(R.id.standard_label);

        ImageView back = findViewById(R.id.backIV);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToProducts();
            }
        });

        simulation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoSimulation();
            }
        });

        comparison.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoComparison();
            }
        });

        standard_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                standard_label.setTextColor(getResources().getColor(R.color.text_light_blue));
                product_label.setTextColor(getResources().getColor(R.color.white));
                videoView.stopPlayback();
                videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video));
                videoView.start();
            }
        });

        product_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product_label.setTextColor(getResources().getColor(R.color.text_light_blue));
                standard_label.setTextColor(getResources().getColor(R.color.white));
                videoView.stopPlayback();
                videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.small));
                videoView.start();
            }
        });

    }


    public void goToProducts(){
        finish();
    }

    public void gotoSimulation() {

        Intent intent = new Intent(context, ProductSimulation.class);
        intent.putExtra(category_name,name);
        context.startActivity(intent);
        finish();
    }

    public void gotoComparison() {

        Intent intent = new Intent(context, SimulationComparison.class);
        intent.putExtra(category_name,name);
        context.startActivity(intent);
        finish();
    }

}