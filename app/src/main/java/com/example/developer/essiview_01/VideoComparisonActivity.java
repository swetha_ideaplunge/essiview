package com.example.developer.essiview_01;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.VideoView;

public class VideoComparisonActivity extends AppCompatActivity {
    VideoView video1, video2;
    String category_name = "Category";
    String name;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_video_comparison);

        Bundle args = getIntent().getExtras();
        if (args != null)
            name = args.getString(category_name);

        ImageView videoActivity = findViewById(R.id.videoIV);

        ImageView back = findViewById(R.id.backIV);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToVideo();
            }
        });

        video1 = findViewById(R.id.video1);
        video2 = findViewById(R.id.video2);

        video1.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.small));

        video2.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video));

        video1.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                video1.start();
                mediaPlayer.setVolume(0,0);
                mediaPlayer.setLooping(true);
            }
        });

        video2.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                video2.start();
                mediaPlayer.setVolume(0,0);
                mediaPlayer.setLooping(true);
            }
        });

        videoActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VideoComparisonActivity.this,ProductVideoActivity.class);
                intent.putExtra(category_name, name);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }

    public void goToVideo(){
        finish();
    }

}
