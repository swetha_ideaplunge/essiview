package com.example.developer.essiview_01.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Xfermode;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.example.developer.essiview_01.R;

public class InnerLayout extends FrameLayout {

    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Xfermode pdMode = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);
    private Path path = new Path();
    Context context;
    private int res_id = R.drawable.peripheral_lens_image;
    private boolean innerImg = false;

    public InnerLayout(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    public InnerLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public InnerLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public InnerLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int saveCount = canvas.saveLayer(0, 0, getWidth(), getHeight(), null, Canvas.ALL_SAVE_FLAG);
        super.dispatchDraw(canvas);

        paint.setXfermode(pdMode);
        path.reset();

            canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), res_id), 100, 0, paint);
        /*else
         canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.peripheral_lens_image), 100, 0, paint);*/

        /*if (innerImg){
            switch (res_id){
                case 1:
                    canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.inner_lens_near_filled), 150, 50, paint);
                    break;
                case 2:
                    canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.inner_lens_mid_filled), 150, 50, paint);
                    break;
                case 3:
                    canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.inner_lens_far_filled), 150, 50, paint);
                    break;
            }

        }else {
            switch (res_id) {
                case 1:
                    canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.inner_lens_near), 150, 50, paint);
                    break;
                case 2:
                    canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.inner_lens_mid_bottom_filled), 150, 50, paint);
                    break;
                case 3:
                    canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.inner_lens_filled), 150, 50, paint);
                    break;
                case 4:
                    canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.inner_lens_mid), 150, 50, paint);
                    break;
                case 5:
                    canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.inner_lens_far), 150, 50, paint);
                    break;

            }
        }*/

        canvas.restoreToCount(saveCount);
        paint.setXfermode(null);

    }

    public void setResourceID(int id){
        res_id = id;
        invalidate();
    }
}
