package com.example.developer.essiview_01;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.developer.essiview_01.adapter.BenefitAdapter;
import com.example.developer.essiview_01.adapter.FeatureAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductDisplayActivity extends AppCompatActivity {

    @BindView(R.id.benefitRV)
    RecyclerView benefitRV;

    BenefitAdapter adapter;

    @BindView(R.id.featuresRV)
    RecyclerView featuresRV;

    FeatureAdapter featureAdapter;

    @BindView(R.id.back_icon)
    ImageView back;

    @BindView(R.id.simulation)
    ImageView simulation;

    @BindView(R.id.label)
    TextView logo;


    int logoId;
    String name;

    String[] benefits;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_product_display);

        ButterKnife.bind(this);

        Bundle args = getIntent().getExtras();
        if (args != null){
            logoId = args.getInt("logo");
            name = args.getString("name");

        }
        name = "Liberty";

        logo.setText(logoId);
        //getData();

//        logo.setImageResource(logoId);

        CustomLayoutManager manager = new CustomLayoutManager(this);
        benefitRV.setLayoutManager(manager);

        benefitRV.setNestedScrollingEnabled(false);

        benefits = getResources().getStringArray(R.array.ultra_benefits);

        List<String> benefitList = Arrays.asList(benefits);

        adapter = new BenefitAdapter(this,benefitList,getIds());

        benefitRV.setAdapter(adapter);

        CustomLayoutManager manager1 = new CustomLayoutManager(this, LinearLayoutManager.HORIZONTAL,false);

        featuresRV.setLayoutManager(manager1);

        featuresRV.setNestedScrollingEnabled(false);

        featureAdapter = new FeatureAdapter(this);
        featuresRV.setAdapter(featureAdapter);
    }

    List<Integer> getIds(){
        List<Integer> list = new ArrayList<>();
        list.add(R.drawable.benefits_1);
        list.add(R.drawable.benefits_2);
        list.add(R.drawable.benefits_3);
        list.add(R.drawable.benefits_4);
        list.add(R.drawable.benefits_5);
        list.add(R.drawable.benefits_6);
        list.add(R.drawable.benefits_7);
        list.add(R.drawable.benefits_8);

        return list;
    }

    @OnClick(R.id.back_icon)
    public void gotoProducts(){
        finish();
    }

    @OnClick(R.id.simulation)
    public void gotoSimulation(){
        Intent intent = new Intent(ProductDisplayActivity.this,ProductSimulation.class);
        startActivity(intent);
    }

    @OnClick(R.id.comparison)
    public void gotoSimulationComparison(){
        Intent intent = new Intent(ProductDisplayActivity.this,SimulationComparison.class);
        startActivity(intent);
    }

    @OnClick(R.id.video)
    public void gotoVideo(){
        Intent intent = new Intent(ProductDisplayActivity.this,ProductVideoActivity.class);
        intent.putExtra("Category","Liberty");
        startActivity(intent);
    }


    void getData(){
        if (name.equalsIgnoreCase("Liberty")){
            benefits = getResources().getStringArray(R.array.ultra_benefits);

        }else if (name.equalsIgnoreCase("Comfort")){
            benefits = getResources().getStringArray(R.array.comfort_benefits);

        } else if (name.equalsIgnoreCase("Physio")){{
            benefits = getResources().getStringArray(R.array.comfort_benefits);

        }}else if (name.equalsIgnoreCase("SSeries")){{
            benefits = getResources().getStringArray(R.array.x_series_benefits);

        }}else if (name.equalsIgnoreCase("S4D")){{
            benefits = getResources().getStringArray(R.array.x_series_benefits);

        }}else if (name.equalsIgnoreCase("DesignShort")){{
            benefits = getResources().getStringArray(R.array.x_series_benefits);

        }}else if (name.equalsIgnoreCase("DesignFit")){{
            benefits = getResources().getStringArray(R.array.x_series_benefits);

        }}else if (name.equalsIgnoreCase("XClusive")){{
            benefits = getResources().getStringArray(R.array.x_series_benefits);

        }}
    }
}
