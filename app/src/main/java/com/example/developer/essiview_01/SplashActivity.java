package com.example.developer.essiview_01;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

public class SplashActivity extends AppCompatActivity {

    ImageView image3 ;
    ImageView image2 ;
    ImageView mesh ;
    ConstraintLayout layout ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        image3 = findViewById(R.id.image3);
        image2 = findViewById(R.id.image2);
        mesh = findViewById(R.id.mesh);
        layout = findViewById(R.id.layout);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this,VariluxActivity.class);
                startActivity(intent);
                finish();
            }
        },3000);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        image2.setImageBitmap(null);
        image3.setImageBitmap(null);
        mesh.setImageBitmap(null);
        layout.setBackground(null);

        image3 = null;
        image2 = null;
        mesh = null;
        layout = null;

    }
}
