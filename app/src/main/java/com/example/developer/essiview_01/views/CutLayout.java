package com.example.developer.essiview_01.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Xfermode;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.example.developer.essiview_01.R;

public class CutLayout extends FrameLayout {

    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Xfermode pdMode = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);
    private Path path = new Path();
    Context context;
    int outerLensID = R.drawable.lens_outer;

    public CutLayout(Context context) {
        this(context, null);
        this.context = context;
    }

    public CutLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        this.context = context;
    }

    public CutLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CutLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int saveCount = canvas.saveLayer(0, 0, getWidth(), getHeight(), null, Canvas.ALL_SAVE_FLAG);
        super.dispatchDraw(canvas);


        paint.setXfermode(pdMode);
        path.reset();


        canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.lens_new_mask),100,0,paint);
        canvas.restoreToCount(saveCount);
        paint.setXfermode(null);

        canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(),R.drawable.lens_new_outer),100,0,paint);

    }

    public void setOuterLensID(int id){
        outerLensID = id;
        invalidate();
    }
}