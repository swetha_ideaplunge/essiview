package com.example.developer.essiview_01;

import java.util.ArrayList;

public class ProductCatalog {

    public static int[] classic = new int[]{R.string.varilux_liberty,R.string.varilux_comfort,R.string.varilux_physio};

    public static int[] eSeries = new int[]{R.string.varilux_eseries};

    public static int[] sSeries = new int[]{R.string.varilux_sseries,R.string.varilux_s4d};

    public static int[] xSeries = new int[]{R.string.varilux_x_design_short,R.string.varilux_x_design_fit,R.string.varilux_xclusive};

}
