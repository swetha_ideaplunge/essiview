package com.example.developer.essiview_01.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.developer.essiview_01.ProductDisplayActivity;
import com.example.developer.essiview_01.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ItemClickListener itemClickListener;
    int[] logo;

    public ProductAdapter(Context ctx, ItemClickListener listener, int[] logos) {
        this.context = ctx;
        this.itemClickListener = listener;
        this.logo = logos;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(context);

        switch (viewType) {
            case 0:
                View view = inflater.inflate(R.layout.adapter_product_item, parent, false);
                viewHolder = new ProductViewHolder(view);
                break;
            case 1:
                View view1 = inflater.inflate(R.layout.adapter_product_opp_item, parent, false);
                viewHolder = new ProductOppViewHolder(view1);
                break;
            default:
                View view2 = inflater.inflate(R.layout.adapter_product_item, parent, false);
                viewHolder = new ProductViewHolder(view2);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        int viewType = getItemViewType(position);
        int img = logo[position];

        switch (viewType) {
            case 0:
                ((ProductViewHolder)holder).logo.setText(img);
                break;
            case 1:
                ((ProductOppViewHolder)holder).logo.setText(img);
                break;
            default:
                ((ProductViewHolder)holder).logo.setText(img);
                break;
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return logo.length;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return 0;
        if (position % 2 ==1)
            return 1;
        else
            return 0;
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.label)
        TextView logo;


        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    class ProductOppViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.label)
        TextView logo;

        public ProductOppViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public interface ItemClickListener {

        public void onClick(int position);
    }
}
