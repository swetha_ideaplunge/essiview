package com.example.developer.essiview_01;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.developer.essiview_01.adapter.BenefitAdapter;
import com.example.developer.essiview_01.adapter.FeatureAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ESeriesFragment extends Fragment {

    @BindView(R.id.toolBar)
    Toolbar toolbar;

    @BindView(R.id.benefitRV)
    RecyclerView benefitRV;

    BenefitAdapter adapter;

    @BindView(R.id.featuresRV)
    RecyclerView featuresRV;

    FeatureAdapter featureAdapter;

    @BindView(R.id.label)
    TextView logo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_product_display,container,false);

        ButterKnife.bind(this,view);

        toolbar.setVisibility(View.GONE);

        logo.setText(R.string.varilux_eseries);

        CustomLayoutManager manager = new CustomLayoutManager(getActivity());
        benefitRV.setLayoutManager(manager);

        benefitRV.setNestedScrollingEnabled(false);

        String[] benefits = getResources().getStringArray(R.array.e_series_benefits);

        List<String> benefitList = Arrays.asList(benefits);

        adapter = new BenefitAdapter(getActivity(),benefitList,getIds());

        benefitRV.setAdapter(adapter);

        CustomLayoutManager manager1 = new CustomLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,false);

        featuresRV.setLayoutManager(manager1);

        featuresRV.setNestedScrollingEnabled(false);

        featureAdapter = new FeatureAdapter(getActivity());
        featuresRV.setAdapter(featureAdapter);
        return view;
    }


    List<Integer> getIds(){
        List<Integer> list = new ArrayList<>();
        list.add(R.drawable.benefits_1);
        list.add(R.drawable.benefits_2);
        list.add(R.drawable.benefits_3);
        list.add(R.drawable.benefits_4);
        list.add(R.drawable.benefits_5);
        list.add(R.drawable.benefits_6);

        return list;
    }

    @OnClick(R.id.simulation)
    public void gotoSimulation(){
        Intent intent = new Intent(getActivity(),ProductSimulation.class);
        getActivity().startActivity(intent);
    }

    @OnClick(R.id.comparison)
    public void gotoSimulationComparison(){
        Intent intent = new Intent(getActivity(),SimulationComparison.class);
        getActivity().startActivity(intent);
    }

    @OnClick(R.id.video)
    public void gotoVideo(){
        Intent intent = new Intent(getActivity(),ProductVideoActivity.class);
        intent.putExtra("Category","ESeries");
        getActivity().startActivity(intent);
    }
}
