package com.example.developer.essiview_01;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VariluxActivity extends AppCompatActivity {

    @BindView(R.id.nextIV)
    ImageView next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_varilux);

        ButterKnife.bind(this);


       /* ImageView image3 = findViewById(R.id.image3);
        ImageView image2 = findViewById(R.id.image2);
        ImageView mesh = findViewById(R.id.mesh);
        ConstraintLayout layout = findViewById(R.id.layout);

       *//* Glide.with(this)
                .load(R.drawable.splash_child_image)
                .into(image3);*//*


       image3.setImageBitmap(decodeSampledBitmapFromResource(getResources(),R.drawable.splash_child_image,200,200));
       image2.setImageBitmap(decodeSampledBitmapFromResource(getResources(),R.drawable.essiview_icon_3_30,100,100));
       mesh.setImageBitmap(decodeSampledBitmapFromResource(getResources(),R.drawable.mesh_for_splash_screen_1_25,200,200));*/

    }

    @OnClick(R.id.nextIV)
    public void goToAdScreen(){
        Intent intent = new Intent(VariluxActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }
}
