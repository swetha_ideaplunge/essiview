package com.example.developer.essiview_01.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.developer.essiview_01.FemaleFragment;
import com.example.developer.essiview_01.MaleFragment;
import com.example.developer.essiview_01.R;

public class GenderAdapter extends FragmentStatePagerAdapter {

    Context context;

    public GenderAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new MaleFragment();
            case 1:
                return new FemaleFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }


}
