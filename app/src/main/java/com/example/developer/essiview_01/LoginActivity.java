package com.example.developer.essiview_01;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {


    @BindView(R.id.loginBtn)
    Button loginBtn;

    @BindView(R.id.usernameET)
    EditText username;

    @BindView(R.id.passwordET)
    EditText password;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

    }

    @OnClick(R.id.loginBtn)
    public void validateCredentials(){
        if (username.getText().toString().equals("demo@demo.com") && password.getText().toString().equals("qwerty")) {
            //makeAPICall();
        Intent intent = new Intent(LoginActivity.this,AdScreenActivity.class);
        startActivity(intent);
        finish();
        }else{
            Toast.makeText(LoginActivity.this, "Invalid Username/Password", Toast.LENGTH_SHORT).show();
        }
    }

    void makeAPICall(){

        RequestQueue queue = Volley.newRequestQueue(this);

        String name = username.getText().toString();
        String pwd = password.getText().toString();
        String url = "http://139.162.44.16/sg/api/api/auth/login";

        JSONObject object = new JSONObject();
        try {
            object.put("email",name);
            object.put("password",pwd);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Intent intent = new Intent(LoginActivity.this,AdScreenActivity.class);
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(LoginActivity.this, "Invalid Username/Password", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(request);


    }
}
